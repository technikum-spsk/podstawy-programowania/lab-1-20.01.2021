#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

void int_cin_fail_clear();

void assign_random_number(int array[10]);

void display_file(fstream &file, const string &file_name);

int main() {
    srand(time(nullptr));
    fstream file, file2;

    int choice, t1[10], t2[10];

    do {
        cout << "1. Wpisz losowe liczby do \"numbers.txt\"" << endl;
        cout << R"(2. Zapisz dodawanie tablic "t1" oraz "t2" do pliku "t1+t2.txt")" << endl;
        cout << "3. Wyswietl plik \"numbers.txt\"" << endl;
        cout << "4. Wyswietl plik \"t1+t2.txt\"" << endl;
        cout << "5. Zamknij program" << endl;
        cin >> choice;

        while (choice < 1 || choice > 5 || cin.fail()) {
            cout << "Wybrano niepoprawna opcje" << endl;
            int_cin_fail_clear();
            cin >> choice;
        }

        if (choice == 1) {
            file.open("numbers.txt", ios::out);
            if (file.good()) {
                assign_random_number(t1);
                assign_random_number(t2);
                file << "Tabela 1   Tabela 2";
                for (int i = 0; i < 10; i++) {
                    file << "\n" << t1[i];
                    for (int j = 0; j < 11 - to_string(t1[i]).length(); j++) {
                        file << " ";
                    }
                    file << t2[i];
                }
            } else {
                cout << "Plik \"numbers.txt\" nie istnieje." << endl;
            }

            file.close();
        }

        if (choice == 2) {
            string word;
            int i = 0;
            file.open("numbers.txt", ios::in);
            if (file.good()) {
                file.seekg(21);
                file2.open("t1+t2.txt", ios::out);
                file2 << "Wynik t1+t2";
                while (!file.eof()) {
                    file >> word;
                    t1[i] = stoi(word);
                    file >> word;
                    t2[i] = stoi(word);
                    file2 << "\n" << t1[i] << " + " << t2[i] << " = " << t1[i] + t2[i];
                    i++;
                }
            } else {
                cout << R"(Plik z tablicami "t1" oraz "t2" nie istnieje.)" << endl;
            }
            file.close();
            file2.close();
        }

        if (choice == 3) {
            file.open("numbers.txt", ios::in);
            display_file(file, "numbers");
        }

        if (choice == 4) {
            file.open("t1+t2.txt", ios::in);
            display_file(file, "t1+t2");
        }

    } while (choice != 5);

    return 0;
}

void int_cin_fail_clear() {
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

void assign_random_number(int array[]) {
    for (int i = 0; i < 10; i++) {
        array[i] = rand() % 10 + 1;
    }
}

void display_file(fstream &file, const string &file_name) {
    string line;
    if (file.good()) {
        getline(file, line);
        int first_line_length = line.length();
        for (int i = 0; i < first_line_length + 1; i++) {
            cout << "-";
        }
        cout << endl;
        file.seekg(0);
        while (!file.eof()) {
            getline(file, line);
            cout << line << endl;
        }
        for (int i = 0; i < first_line_length + 1; i++) {
            cout << "-";
        }
        cout << endl;
        system("pause");
    } else {
        cout << "Plik \"" << file_name << ".txt\" nie istnieje." << endl;
    }
    cout << endl;
    file.close();
}